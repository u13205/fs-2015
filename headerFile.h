  #include<stdio.h>
  #include<stdlib.h>
  #include<string.h>
  #include <sys/types.h>
#include <sys/stat.h>
 #include <fcntl.h>

typedef struct DiskNode
{
	unsigned int prev;
	unsigned int next;
	unsigned int data;
}node;

typedef struct DiskListHead
{
	unsigned int head;
	unsigned int listid;
	unsigned int prev;
	unsigned int next;
}list;

typedef struct Disk
{
	char *path;
	unsigned int freeid;
	unsigned int diskid;
}disk;
int deletelist(disk *diskId,unsigned int listid);
int deletebyval(disk *diskId,unsigned int listid,unsigned int data);
int deletebypos(disk *diskId,unsigned int listid,unsigned int pos);
int createlist(disk *diskId,unsigned int listid);
int insert(disk *diskId,unsigned int listid,unsigned int pos, unsigned int val);
int myatoi(int *num,char*string,int index);
int mysscanf(char *buffer,char *inst,int *diskid,int *listid,int *pos,int *val,int *count);
int isfreenodepresent(int fd);

int writelist(int fd,list l,unsigned int addr);
int writenode(int fd,node l,unsigned int addr);
int readlist(int fd,list *l,unsigned int addr);
int readnode(int fd,node *l,unsigned int addr);

int displaynodes(disk *mydisk,unsigned int listid);
