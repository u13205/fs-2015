#include"headerFile.h"
int deletebyval(disk * diskId,unsigned int listid,unsigned int data)
{
	list _list={0},_dlist={0};
	node _node={0},_next={0},_prev={0};
	unsigned int fd,laddr=0,naddr=0,pos=1;;
	fd=open(diskId->path,O_RDWR);
	lseek(fd,0,SEEK_SET);
	read(fd,&laddr,3);
	do{
		readlist(fd,&_list,laddr);
		if(_list.listid==listid)
			break;
		if(_list.next==0x0)
			return 1;
		laddr=_list.next;
	}while(1);

	if(_list.head==0x0)
		return 1;
	readlist(fd,&_dlist,3);
	naddr=_list.head;	
	do{
		readnode(fd,&_node,naddr);
		if(_node.data==data)
		{
			if(pos==1)
			{
				_list.head=_node.next;
				_node.next=_dlist.head;
				_node.prev=0x0;
				_dlist.head=naddr;

				writelist(fd,_dlist,3);
				writelist(fd,_list,laddr);
				writenode(fd,_node,naddr);
			
				naddr=_list.head;
			}
			else
			{
				if(_node.next!=0x0)
				{
					readnode(fd,&_prev,_node.prev);
					readnode(fd,&_next,_node.next);
					_prev.next=_node.next;
					_next.prev=_node.prev;
					writenode(fd,_prev,_node.prev);
					writenode(fd,_next,_node.next);
					_node.prev=0x0;
					_node.next=_dlist.head;
					_dlist.head=naddr;
					writelist(fd,_dlist,16);
					writenode(fd,_node,naddr);	
				}
				else
				{
					readnode(fd,&_prev,_node.prev);
					_prev.next=_node.next;
					writenode(fd,_prev,_node.prev);
					_node.prev=0x0;
					_node.next=_dlist.head;
					_dlist.head=naddr;
					writelist(fd,_dlist,3);
					writenode(fd,_node,naddr);
				}
				naddr=_prev.next;
			}

		}
		else
		{
			pos++;
			naddr=_node.next;
		}
	}while(naddr!= 0x0 );
	return 0;
}

