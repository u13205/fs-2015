#include"headerFile.h"
int deletelist(disk *diskId,unsigned int listid)
{
	list _list={0},_dlist={0},_prevlist={0},_nextlist={0};
	node _node={0},_prev={0},_next={0};
	unsigned int fd,laddr=0,naddr=0,check=0;

	fd=open(diskId->path,O_RDWR);
	lseek(fd,0,SEEK_SET);
	read(fd,&laddr,3);
	do
	{
		readlist(fd,&_list,laddr);
		if(_list.listid==listid)
			break;
		if(_list.next==0x0)
			return 1;
		laddr=_list.next;
	}while(1);

	if(_list.prev==0x0 && _list.next==0x0)
	{
		lseek(fd,0,SEEK_SET);
		laddr=0x0;
		write(fd,&laddr,3);

	}
	else if(_list.prev==0x0 && _list.next!=0x0)
	{
		//readlist(fd,&_prevlist,list.prev);
		lseek(fd,0,SEEK_SET);
		read(fd,&check,3);
		if(check==laddr)
		{	
			lseek(fd,0,SEEK_SET);
			write(fd,&_list.next,3);
		}
		readlist(fd,&_nextlist,_list.next);
		_nextlist.prev=0x0;
		writelist(fd,_nextlist,_list.next);
	}
	else if(_list.prev!=0x0 && _list.next==0x0)
	{
		readlist(fd,&_prevlist,_list.prev);
		_prevlist.next=0x0;
		writelist(fd,_prevlist,_list.prev);
	}
	else if(_list.prev!=0x0 && _list.next!=0x0)
	{
		readlist(fd,&_prevlist,_list.prev);
		readlist(fd,&_nextlist,_list.next);
		_prevlist.next=_list.next;
		_nextlist.prev=_list.prev;
		writelist(fd,_prevlist,_list.prev);
		writelist(fd,_nextlist,_list.next);
	}
	readlist(fd,&_dlist,3);
	naddr=_list.head;
	_list.next=_dlist.next;
	_list.prev=3;
	if(_dlist.next!=0x0)
	{
		readlist(fd,&_nextlist,_list.next);
		_nextlist.prev=laddr;
		writelist(fd,_nextlist,_list.next);
	}
	writelist(fd,_dlist,3);
	writelist(fd,_list,laddr);

	//hadling list nodes
	if(_list.head!=0x0)
	{
		readlist(fd,&_dlist,3);
		if(_dlist.head==0x0)
		{
			_dlist.head=_list.head;
			write(fd,_dlist,3);
			return 0;
		}
		else{
			laddr=_dlist.head;
			_dlist.head=_list.head;
		
			naddr=_list.head;
			do{
				readnode(fd,&_node,naddr);
				if(_node.next==0x0)
					break;
				naddr=_node.next;
			}while(1);	
			_node.next=laddr;
			writenode(fd,_node,naddr);
			readnode(fd,&_node,laddr);
			_node.prev=naddr;
			writenode(fd,_node,laddr);
			writelist(fd,_dlist,3);
		}
	}
	
	return 0;	
}
