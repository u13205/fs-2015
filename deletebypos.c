#include"headerFile.h"
int deletebypos(disk *diskId,unsigned int listid,unsigned int pos)
{
	list _list={0};
	node _node={0},_next={0};
	int fd,laddr,naddr;
	fd=open(diskId->path,O_RDWR);
	read(fd,&laddr,3);
	do{
		readlist(fd,&_list,laddr);
		if(_list.listid==listid)
			break;
		if(_list.next==0x0)
			return 1;
		laddr=_list.next;
	}while(1);
	
	if(_list.head==0x0)
		return 1;
	naddr=_list.head;
	if(pos==1)
	{
		readnode(fd,&_node,naddr);
		_list.head=_node.next;
		writelist(fd,_list,laddr);
	}
	else
	{
		pos--;
		while(pos)
		{
			readnode(fd,&_node,naddr);
			if(pos==1)
			{
				laddr=naddr;
				naddr=_node.next;
				readnode(fd,&_next,_node.next);
				_node.next=_next.next;
				writenode(fd,_node,laddr);
				if(_next.next!=0x0)
				{
					readnode(fd,&_next,_node.next);
					_next.prev=laddr;
					writenode(fd,_next,_node.next);
				}
				break;
			}
			if(_node.next==0x0)
				return 1;
			naddr=_node.next;
			pos--;
		}
		
	}

	readlist(fd,&_list,3);
	if(_list.head==0x0)
	{
		_list.head=naddr;
		writelist(fd,_list,3);
		readnode(fd,&_node,naddr);
		_node.next=_node.prev=_node.data=0x0;
		writenode(fd,_node,naddr);
		return 0;	
	}
	else
	{
		_node.data=_node.prev=0x0;
		_node.next=_list.head;
		writenode(fd,_node,naddr);
		_list.head=naddr;
		writelist(fd,_list,3);
		return 0;
	}
}
