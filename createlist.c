	#include"headerFile.h"
int writelist(int fd,list l,unsigned int addr)
{
	lseek(fd,addr,SEEK_SET);
	write(fd,&(l.head),3);
	write(fd,&(l.listid),4);
	write(fd,&(l.prev),3);
	write(fd,&(l.next),3);
	return 0;
}
int writenode(int fd,node l,unsigned int addr)
{
	lseek(fd,addr,SEEK_SET);
	write(fd,&(l.prev),3);
	write(fd,&(l.next),3);
	write(fd,&(l.data),4);
	return 0;
}

int readlist(int fd,list *l,unsigned int addr)
{
	lseek(fd,addr,SEEK_SET);
	read(fd,&(l->head),3);
	read(fd,&(l->listid),4);
	read(fd,&(l->prev),3);
	read(fd,&(l->next),3);
	return 0;
}
int readnode(int fd,node *l,unsigned int addr)
{
	lseek(fd,addr,SEEK_SET);
	read(fd,&(l->prev),3);
	read(fd,&(l->next),3);
	read(fd,&(l->data),4);
	return 0;
}
int displaynodes(disk *mydisk,unsigned int listid)
{
	int fd;
	list _list={0};
	node _node={0};
	int addr=0;
	fd=open(mydisk->path,O_RDONLY);
	lseek(fd,0,SEEK_SET);
	//printf("%d\n",read(fd,&addr,3));
	read(fd,&addr,3);
	do{
		if(addr==0x0)
			return 1;
		readlist(fd,&_list,addr);
		addr=_list.next;
	}while(_list.listid!=listid);
	
	addr=_list.head;
	printf("\nlistid( %d ):\t",listid);
	while(addr!=0x0)
	{
		readnode(fd,&_node,addr);
		printf("[ %d %d  %d ]",_node.prev,_node.next,_node.data);
		addr=_node.next;
	}
	return 0;
}
int createlist(disk *diskId,unsigned int listid)
{       
	int fd=0,addr=0,newlistaddr=0;
	list _list={0},_new={0};
	fd=open(diskId->path,O_RDWR);
	lseek(fd,0,SEEK_SET);
	read(fd,&addr,3);
	if(isfreelistpresent(fd))
	{
		if(addr==0x0)
		{
			addr=diskId->freeid;
			//close(fd);
			//fd=open(diskId.path,O_RDWR);
			lseek(fd,0,SEEK_SET);
			write(fd,&addr,3);
		
			_list.head	=0x0;
			_list.listid=listid;
			_list.next	=0x0;
			_list.prev	=0x0;
			writelist(fd,_list,addr);
			diskId->freeid += 13;
			return 0;
		}
		else
		{
			do
			{
				readlist(fd,&_list,addr);
				if(_list.listid==listid)
					return 1;
				if(_list.next==0x0)
					break;
				addr = _list.next;
			}while(1);

			_list.next = diskId->freeid;
		
			writelist(fd,_list,addr);
		
			_new.head	=0x0;
			_new.listid=listid;
			_new.next	=0x0;
			_new.prev	= addr;
			writelist(fd,_new,diskId->freeid);
			diskId->freeid += 13;
			return 0;
		}	
	}
	else
	{
		addr = 3;
		
		readlist(fd,&_list,addr);
		newlistaddr=_list.next;

		readlist(fd, &_new,_list.next);
		_list.next=_new.next;	
		
		readlist(fd, &_new,_new.next);
		_new.prev=3;
		writelist(fd,_new,_new.next);
		writelist(fd,_list,3);

		addr=29;
		
		do
		{
			readlist(fd,&_list,addr);
			if(_list.listid==listid)
				return 1;
			if(_list.next==0x0)
				break;
			addr = _list.next;
		}while(1);

		_list.next = newlistaddr;

		_list.head	=0x0;
		_list.listid=listid;
		_list.next	=0x0;
		_list.prev	= addr;
		writelist(fd,_list,newlistaddr);
		return 0;
	}
}

int isfreelistpresent(int fd)
{
	list _list={0};
	readlist(fd,&_list,3);
	if(_list.next==0)
		return 1;
	else
		return 0;
}
int isfreenodepresent(int fd)
{
	list _list={0};
	readlist(fd,&_list,3);
	if(_list.head==0)
		return 1;
	else
		return 0;
}
